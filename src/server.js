import Web3 from 'web3';
import AWSHttpProvider from './middleware/aws-http-provider.js';
import * as dotenv from 'dotenv';

dotenv.config();

const endpoint = process.env.AMB_HTTP_ENDPOINT;

const web3 = new Web3(new AWSHttpProvider(endpoint));

web3.eth.getNodeInfo().then((res) => {
  console.log('Node info: ', res);
});
