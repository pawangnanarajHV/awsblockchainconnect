import Web3 from 'web3';

const web3 = new Web3('HTTP://127.0.0.1:7545');

web3.eth.getNodeInfo().then((res) => {
  console.log('Node info: ', res);
});

const accountsArray = await web3.eth.getAccounts();
console.log('array :', accountsArray);

const account = web3.eth.accounts.create();
console.log('account: ', account);

accountsArray.push(account);
console.log('array :', accountsArray);

const newAccountsArray = await web3.eth.getAccounts();
console.log('array :', newAccountsArray);
